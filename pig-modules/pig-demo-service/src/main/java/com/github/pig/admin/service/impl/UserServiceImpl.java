package com.github.pig.admin.service.impl;

import com.github.pig.admin.entity.User;
import com.github.pig.admin.mapper.UserMapper;
import com.github.pig.admin.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gaolei
 * @since 2018-12-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
