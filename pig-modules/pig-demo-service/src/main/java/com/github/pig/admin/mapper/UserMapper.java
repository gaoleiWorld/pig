package com.github.pig.admin.mapper;

import com.github.pig.admin.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gaolei
 * @since 2018-12-10
 */
public interface UserMapper extends BaseMapper<User> {

}
