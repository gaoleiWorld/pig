package com.github.pig.admin.service;

import com.github.pig.admin.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gaolei
 * @since 2018-12-10
 */
public interface IUserService extends IService<User> {

}
